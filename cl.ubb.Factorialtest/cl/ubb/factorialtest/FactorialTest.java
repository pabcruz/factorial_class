package cl.ubb.factorialtest;
import static org.junit.Assert.*;
import org.junit.Test;

import cl.ubb.Factorial.Factorial;

public class FactorialTest {
	
	@Test
	public void validarFactorialCeroEsCero(){
		
		//Arrange 
		Factorial factorial=new Factorial();
		int  resultado;
		//Act
		resultado=factorial.calcularFactorial(0);
		
		//Assert
		assertEquals(resultado,0);
	}

	@Test
	public void validarFactorialUnoEsUno(){
		//Arrange
		Factorial factorial=new Factorial();
		int resultado;
		
		//Act
		resultado=factorial.calcularFactorial(1);
		
		//Assert
		assertEquals(resultado, 1);
	}
	
	@Test
	public void validarFactorialDosEsDos(){
		//Arrange
		Factorial factorial=new Factorial();
		int resultado;
		
		//Act
		resultado=factorial.calcularFactorial(2);
		
		//Assert
		assertEquals(resultado,2);
		
	}
	
	@Test
	public void validarFactorialTresEsSeis(){
		//Arrange
		Factorial factorial=new Factorial();
		int resultado;				
		
		//Act
		resultado=factorial.calcularFactorial(3);
				
		//Assert
		assertEquals(resultado,6);
	}
	
	@Test
	public void validarFactorialCuatroEsVeinticuatro(){
		
		//Arrange
				Factorial factorial=new Factorial();
				int resultado;				
				
				//Act
				resultado=factorial.calcularFactorial(4);
						
				//Assert
				assertEquals(resultado,24);
	}
}
